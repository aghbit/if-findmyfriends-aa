package pl.edu.bit.aa.rest.json;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;

@Value
public class UserIdJson {
    private final String userId;

    @JsonCreator
    public UserIdJson(@JsonProperty("userId") String userId){
        this.userId = userId;
    }

}
