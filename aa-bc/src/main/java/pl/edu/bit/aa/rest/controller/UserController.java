package pl.edu.bit.aa.rest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import pl.edu.bit.aa.entity.Permission;
import pl.edu.bit.aa.entity.Role;
import pl.edu.bit.aa.entity.User;
import pl.edu.bit.aa.rest.json.*;
import pl.edu.bit.aa.rest.util.Authenticated;
import pl.edu.bit.aa.rest.util.Authorized;
import pl.edu.bit.aa.service.UserService;
import pl.edu.bit.aa.service.exception.*;
import javax.validation.Valid;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@RestController
@RequestMapping
class UserController {

    @Autowired
    private UserService userService;

    @RequestMapping(
            value = "/users",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public CreatedUserJson createUser(@Valid @RequestBody SimpleCredentialsJson credentials)
            throws AlreadyExistingUserException, PasswordTooWeakException, InvalidEmailException {
        final UUID userId = userService.createNewUser(credentials.getName(), credentials.getSurname(), credentials.getEmail(), credentials.getPassword());
        return new CreatedUserJson(credentials.getEmail(), userId.toString());
    }

    @RequestMapping(
            value = "/users",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    @Authorized(Permission.ADMIN)
    public UsersListJson getUsers() {
        List<User> users = userService.getAllUsers();
        return UsersListJson.create(
                users.stream().map(user -> UserJson.create(
                        user.getId().toString(),
                        user.getName(),
                        user.getSurname(),
                        user.getEmail(),
                        user.getRoles().stream().map(Role::getName).collect(Collectors.toList())
                )).collect(Collectors.toList()));
    }

    @RequestMapping(
            value = "/users/{userId}/password",
            method = RequestMethod.PATCH,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @Authenticated
    public void changePassword(@PathVariable String userId, @Valid @RequestBody PasswordChangeJson body)
            throws FailedPasswordChangeException, PasswordTooWeakException, SamePasswordException {
        userService.changePassword(userId, body.getOldPassword(), body.getNewPassword());
    }

    @RequestMapping(
            value = "/users/{userId}/roles",
            method = RequestMethod.PUT,
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    @Authorized(Permission.ADMIN)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void assignRoleToMember(@PathVariable String userId, @Valid @RequestBody RoleNameJson role) throws NoSuchUserException, NoSuchRoleException {
        userService.assignRoleToUser(userId, role.getRoleName());
    }
}
