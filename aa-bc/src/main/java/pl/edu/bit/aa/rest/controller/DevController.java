package pl.edu.bit.aa.rest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import pl.edu.bit.aa.config.ApplicationProfile;
import pl.edu.bit.aa.db.RolesRepository;
import pl.edu.bit.aa.db.TokenRepository;
import pl.edu.bit.aa.db.UserRepository;
import pl.edu.bit.aa.db.config.DevelopmentDataBaseSeeder;


/**
 * DevController publish endpoint which are helpful for development purpose.
 *
 * This class is not intended to be tested or enabled on production.
 */
@Profile(ApplicationProfile.DEVELOPMENT)
@RestController
@RequestMapping("/dev")
class DevController {

    @Autowired
    private RolesRepository rolesRepository;

    @Autowired
    private TokenRepository tokenRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private DevelopmentDataBaseSeeder seeder;

    @RequestMapping(value = "/flush", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.OK)
    public void flushDatabase() {
        rolesRepository.deleteAll();
        tokenRepository.deleteAll();
        userRepository.deleteAll();
    }

    @RequestMapping(value = "/seed", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.OK)
    public void seedDatabase() {
        seeder.provide();
    }
}
