package pl.edu.bit.aa.rest.config;

import org.springframework.context.annotation.Configuration;
import pl.edu.bit.aa.rest.util.RequestConstant;

import javax.servlet.*;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Filter adds CORS (Cross-Origin Resource Sharing) header that allows requests from front-end server to be processed.
 * This filter accepts all origins, as application is intended to **not** be accessible from regular users, but only
 * from other KN BIT applications.
 */
@Configuration
public class CorsFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        response.setHeader("Access-Control-Allow-Origin", "*");
        response.setHeader("Access-Control-Allow-Credentials", "true");
        response.setHeader("Access-Control-Allow-Methods", "POST, PUT, GET, HEAD, OPTIONS, PATCH");
        response.setHeader("Access-Control-Allow-Headers", RequestConstant.AUTH_HEADER + ", Origin, Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers");
        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {
    }
}
