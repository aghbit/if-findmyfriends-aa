package pl.edu.bit.aa.service;

import com.google.common.collect.Sets;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.edu.bit.aa.db.RolesRepository;
import pl.edu.bit.aa.entity.Permission;
import pl.edu.bit.aa.entity.Role;
import pl.edu.bit.aa.service.exception.AlreadyExistingRoleException;
import pl.edu.bit.aa.service.exception.NoSuchPermissionException;
import pl.edu.bit.aa.service.exception.NoSuchRoleException;
import pl.edu.bit.aa.service.exception.RoleNotFoundException;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class RolesService {

    private final RolesRepository rolesRepository;

    @Autowired
    RolesService(@NonNull RolesRepository rolesRepository) {
        this.rolesRepository = rolesRepository;
    }

    public List<Role> getAllRoles() {
        return rolesRepository.findAll();
    }

    public void createRole(@NonNull String name, @NonNull Set<String> permissionNames)
            throws AlreadyExistingRoleException, NoSuchPermissionException {
        Optional<Role> role = rolesRepository.findRoleByName(name);
        if (role.isPresent()) {
            throw new AlreadyExistingRoleException();
        }

        Set<Permission> permissions = tryConvertOrThrowFrom(permissionNames);

        Role newRole = Role.create(name, permissions);
        rolesRepository.save(newRole);
    }

    /**
     * Tries to convert from name to {@link Permission} instance.
     *
     * @param permissionNames
     * @return
     * @throws NoSuchPermissionException if some permission does not exist.
     */
    private Set<Permission> tryConvertOrThrowFrom(@NonNull Set<String> permissionNames) throws NoSuchPermissionException {
        Set<Permission> permissions = Sets.newHashSet();

        for (String name : permissionNames) {
            Optional<Permission> permission = Permission.find(name);
            if (permission.isPresent()) {
                permissions.add(permission.get());
            } else {
                throw new NoSuchPermissionException(name);
            }
        }

        return permissions;
    }


    public void delete(@NonNull String name) {
        Optional<Role> role = rolesRepository.findRoleByName(name);

        if (role.isPresent()) {
            rolesRepository.delete(role.get());
        }
    }

    public void update(@NonNull String oldName, @NonNull String newName, @NonNull Set<String> newPermissionNames)
            throws AlreadyExistingRoleException, NoSuchPermissionException, RoleNotFoundException {
        Optional<Role> roleToChange = rolesRepository.findRoleByName(oldName);

        if (!roleToChange.isPresent()) {
            throw new RoleNotFoundException();
        }

        if (!oldName.equals(newName) && rolesRepository.findRoleByName(newName).isPresent()) {
            throw new AlreadyExistingRoleException();
        }

        if (!oldName.equals(newName)) {
            rolesRepository.delete(roleToChange.get());
        }

        Set<Permission> newPermissions = tryConvertOrThrowFrom(newPermissionNames);

        rolesRepository.save(Role.create(newName, newPermissions));
    }

    public Role get(@NonNull String name) throws RoleNotFoundException {
        Optional<Role> role = rolesRepository.findRoleByName(name);
        if (role.isPresent()) {
            return role.get();
        } else {
            throw new RoleNotFoundException();
        }
    }
}
