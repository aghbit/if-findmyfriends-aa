package pl.edu.bit.aa.service;

import com.google.common.collect.Sets;
import lombok.NonNull;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.routines.EmailValidator;
import org.jasypt.util.password.PasswordEncryptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.edu.bit.aa.db.RolesRepository;
import pl.edu.bit.aa.db.UserRepository;
import pl.edu.bit.aa.entity.Role;
import pl.edu.bit.aa.entity.User;
import pl.edu.bit.aa.service.exception.*;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

@Service
public class UserService {

    private final UserRepository userRepository;

    private final RolesRepository rolesRepository;

    private final PasswordStrengthPolicy policy;

    private final PasswordEncryptor encryptor;

    @Autowired
    UserService(@NonNull UserRepository userRepository,
                @NonNull RolesRepository rolesRepository,
                @NonNull PasswordStrengthPolicy policy,
                @NonNull PasswordEncryptor encryptor) {
        this.userRepository = userRepository;
        this.rolesRepository = rolesRepository;
        this.policy = policy;
        this.encryptor = encryptor;
    }

    public UUID createNewUser(@NonNull String name, @NonNull String surname, @NonNull String email, @NonNull String password) throws AlreadyExistingUserException, InvalidEmailException, PasswordTooWeakException {
        if (!EmailValidator.getInstance().isValid(email)) {
            throw new InvalidEmailException(email);
        }
        Optional<User> userInDb = userRepository.findByEmail(email);
        if (userInDb.isPresent()) {
            throw new AlreadyExistingUserException(email);
        }

        if (!policy.isPasswordStrong(password)) {
            throw new PasswordTooWeakException();
        }

        UUID userId = UUID.randomUUID();

        while (userRepository.containsId(userId.toString())) {
            userId = UUID.randomUUID();
        }

        User user = User.create(name, surname, email, encryptor.encryptPassword(password));
        userRepository.save(user);
        return user.getId();
    }

    public void changePassword(@NonNull String userId, @NonNull String oldPassword, @NonNull String newPassword)
            throws FailedPasswordChangeException, PasswordTooWeakException, SamePasswordException {
        if (StringUtils.equals(oldPassword, newPassword)) {
            throw new SamePasswordException();
        }

        Optional<User> user = userRepository.findById(userId);
        if (!user.isPresent()) {
            throw new FailedPasswordChangeException();
        }
        if (!encryptor.checkPassword(oldPassword, user.get().getPassword())) {
            throw new FailedPasswordChangeException();
        }
        if (!policy.isPasswordStrong(newPassword)) {
            throw new PasswordTooWeakException();
        }

        User newUser = User.create(
                user.get().getId(),
                user.get().getName(),
                user.get().getSurname(),
                user.get().getEmail(),
                encryptor.encryptPassword(newPassword),
                user.get().getRoles()
        );

        userRepository.save(newUser);
    }

     public List<User> getAllUsers(){
        return userRepository.findAll();
    }

    /**
     * Assigns role in an idempotent manner - i.e. assigning role to user already possessing that role has no effect.
     * @param userId userId
     * @param roleName roleName
     * @throws NoSuchUserException
     * @throws NoSuchRoleException
     */
    public void assignRoleToUser(@NonNull String userId, @NonNull String roleName) throws NoSuchUserException, NoSuchRoleException {
        Optional<User> user = userRepository.findById(userId);
        if (!user.isPresent()) {
            throw new NoSuchUserException();
        }

        Optional<Role> role = rolesRepository.findRoleByName(roleName);
        if (!role.isPresent()) {
            throw new NoSuchRoleException();
        }

        Set<Role> newRoles = Sets.newHashSet(user.get().getRoles());
        if (!newRoles.contains(role.get())) { // if user already contains role, no need to re-create him
            newRoles.add(role.get());
            User newUser = User.create(
                    user.get().getId(),
                    user.get().getName(),
                    user.get().getSurname(),
                    user.get().getEmail(),
                    user.get().getPassword(),
                    newRoles
            );
            userRepository.save(newUser);
        }
    }
}
