package pl.edu.bit.aa.rest.exception;

import lombok.EqualsAndHashCode;
import lombok.Value;
import org.springframework.http.HttpStatus;
import pl.edu.bit.aa.rest.util.exception.JsonException;

@Value
@EqualsAndHashCode(callSuper = true)
@JsonException(HttpStatus.FORBIDDEN)
public class ForbiddenException extends Exception {
    private final String reason = "FORBIDDEN";
}