package pl.edu.bit.aa.db.config;

import com.mongodb.MongoClient;
import lombok.SneakyThrows;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MorphiaConfig {

    @Value("${bit.aa.mongo.host}")
    private String hostname;

    @Value("${bit.aa.mongo.port}")
    private int port;

    @Value("${bit.aa.mongo.db}")
    private String dbName;

    @Bean
    @SneakyThrows
    public Datastore provideDatastore() {
        Datastore ds = new Morphia()
                .mapPackage("pl.edu.bit.aa.entity")
                .createDatastore(new MongoClient(hostname, port), dbName);

        ds.ensureIndexes(); //creates indexes from @Index annotations in your entities
        ds.ensureCaps(); //creates capped collections from @Entity

        return ds;
    }
}
