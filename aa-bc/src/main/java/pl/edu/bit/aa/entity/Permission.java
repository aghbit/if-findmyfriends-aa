package pl.edu.bit.aa.entity;
import lombok.NonNull;
import java.util.Optional;


public enum Permission {

    /**
     * God-permission for admin role as temporary solution.
     */
    ADMIN,
    /**
     * TEST permission is for use <b>in tests only!</b>
     */
    TEST;

    public static Optional<Permission> find(@NonNull String name) {
        try {
            return Optional.of(Permission.valueOf(name));
        } catch (IllegalArgumentException e) {
            return Optional.empty();
        }
    }
}
