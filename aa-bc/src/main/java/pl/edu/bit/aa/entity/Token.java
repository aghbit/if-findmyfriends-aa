package pl.edu.bit.aa.entity;

import lombok.*;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Indexed;
import org.mongodb.morphia.annotations.Reference;

import java.util.Date;

@Data
@EqualsAndHashCode(of = "token")
@RequiredArgsConstructor(staticName = "create")
@Entity(noClassnameStored = true)
public class Token {
    @Id
    private final String token;

    @Reference
    private final User user;

    @Indexed(expireAfterSeconds = TokenExpirationPeriod.DEFAULT_EXPIRATION_TIME)
    @Getter(AccessLevel.NONE)
    private final Date lastUsed = new Date();

    // Needed by MongoDB
    private Token() {
        token = null;
        user = null;
    }
}
