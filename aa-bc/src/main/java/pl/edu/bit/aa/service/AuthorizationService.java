package pl.edu.bit.aa.service;

import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.edu.bit.aa.db.UserRepository;
import pl.edu.bit.aa.entity.Permission;
import pl.edu.bit.aa.entity.User;

import javax.swing.text.html.Option;
import java.util.Optional;

@Service
public class AuthorizationService {

    private final UserRepository userRepository;

    @Autowired
    AuthorizationService(@NonNull UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public boolean userHasPermission(@NonNull String userId, @NonNull String permissionName) {
        Optional<User> user = userRepository.findById(userId);
        if (!user.isPresent()) {
            return false;
        }

        Optional<Permission> desiredPermission = Permission.find(permissionName);

        if (!desiredPermission.isPresent()) {
            return false;
        }

        return user.get().getRoles().stream()
                .flatMap(role -> role.getPermissions().stream())
                .anyMatch(permission -> desiredPermission.get().equals(permission));
    }
}
