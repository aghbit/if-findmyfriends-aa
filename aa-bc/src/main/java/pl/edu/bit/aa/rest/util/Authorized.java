package pl.edu.bit.aa.rest.util;

import pl.edu.bit.aa.entity.Permission;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Marks controller method which should be authorized before actual request handling.
 * It needs {@link Permission} parameter to know which permission to check.
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Authorized {
    Permission value();
}
