package pl.edu.bit.aa.rest.json;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;

@Value
public class SimpleCredentialsJson {
    private final String name;
    private final String surname;
    private final String email;
    private final String password;


    @JsonCreator
    public SimpleCredentialsJson(@JsonProperty("name") String name, @JsonProperty("surname") String surname, @JsonProperty("email") String email, @JsonProperty("password") String password) {
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.password = password;
    }

}