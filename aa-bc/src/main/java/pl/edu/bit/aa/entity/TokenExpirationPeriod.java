package pl.edu.bit.aa.entity;

class TokenExpirationPeriod {
    /**
     * Number of seconds after which token will be automatically removed from DB.
     * Changing it during DB runtime causes mongoDB exceptions about already existing TTL index.
     */
    static final int DEFAULT_EXPIRATION_TIME = 3600;
}
