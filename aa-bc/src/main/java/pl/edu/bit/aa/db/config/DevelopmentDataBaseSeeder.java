package pl.edu.bit.aa.db.config;

import com.google.common.collect.Sets;
import lombok.Data;
import org.jasypt.util.password.PasswordEncryptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;
import pl.edu.bit.aa.config.ApplicationProfile;
import pl.edu.bit.aa.db.RolesRepository;
import pl.edu.bit.aa.db.TokenRepository;
import pl.edu.bit.aa.db.UserRepository;
import pl.edu.bit.aa.entity.Permission;
import pl.edu.bit.aa.entity.Role;
import pl.edu.bit.aa.entity.User;

import javax.annotation.PostConstruct;
import java.util.Collections;
import java.util.Set;
import java.util.UUID;

@Data
@Component
@PropertySource("classpath:development-database-seeder.properties")
@Profile(ApplicationProfile.DEVELOPMENT)
public class DevelopmentDataBaseSeeder {


    @Value("${db.user.name}")
    private String userName;
    @Value("${db.user.surname}")
    private String userSurname;
    @Value("${db.user.email}")
    private String userEmail;
    @Value("${db.user.password}")
    private String userPassword;
    @Value("${db.user.token}")
    private String userTokenString;
    @Value("${db.admin.email}")
    private String adminEmail;
    @Value("${db.admin.password}")
    private String adminPassword;
    @Value("${db.admin.token}")
    private String adminTokenString;
    @Value("${db.admin.name}")
    private String adminName;
    @Value("${db.admin.surname}")
    private String adminSurname;


    @Autowired
    private UserRepository userRepository;
    @Autowired
    private TokenRepository tokenRepository;
    @Autowired
    private RolesRepository rolesRepository;
    @Autowired
    private PasswordEncryptor encryptor;

    @PostConstruct
    public void provide() {
        userRepository.deleteAll();
        tokenRepository.deleteAll();
        rolesRepository.deleteAll();

        Set<Permission> adminPermissions = Sets.newHashSet(Permission.values());
        adminPermissions.remove(Permission.TEST);

        Role adminRole = Role.create("ADMIN", adminPermissions);
        rolesRepository.save(adminRole);

        User user = User.create(
                UUID.randomUUID(),
                userName,
                userSurname,
                userEmail,
                encryptor.encryptPassword(userPassword),
                Collections.emptySet());
        User admin = User.create(
                UUID.randomUUID(),
                adminName,
                adminSurname,
                adminEmail,
                encryptor.encryptPassword(adminPassword),
                Collections.singleton(adminRole));

        User secretAdmin = User.create(
                UUID.randomUUID(),
                "admin",
                "admin",
                "secretAdmin@aa.com",
                encryptor.encryptPassword("adminPass"),
                Collections.singleton(adminRole));

        User topSecretAdmin = User.create(
                UUID.randomUUID(),
                "admin",
                "admin",
                "topSecretAdmin@aa.com",
                encryptor.encryptPassword("adminPass"),
                Collections.singleton(adminRole));

        userRepository.save(secretAdmin);
        userRepository.save(topSecretAdmin);
        userRepository.save(admin);
        userRepository.save(user);
    }

}
