package pl.edu.bit.aa.rest.json;

import lombok.RequiredArgsConstructor;
import lombok.Value;

import java.util.List;

@Value
@RequiredArgsConstructor(staticName = "create")
public class RolesListJson {
    private final List<String> roles;
}
