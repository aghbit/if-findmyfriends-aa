package pl.edu.bit.aa.rest.json;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;

@Value
public class AuthorizationJson {
    private final String permission;

    @JsonCreator
    public AuthorizationJson(@JsonProperty(value = "permission") String permission) {
        this.permission = permission;
    }
}
