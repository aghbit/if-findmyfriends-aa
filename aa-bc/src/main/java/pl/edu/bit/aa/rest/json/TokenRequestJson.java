package pl.edu.bit.aa.rest.json;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;

@Value
public class TokenRequestJson {
    private final String userId;
    private final String password;

    @JsonCreator
    public TokenRequestJson(@JsonProperty("userId") String userId, @JsonProperty("password") String password) {
        this.userId = userId;
        this.password = password;
    }

}
