package pl.edu.bit.aa.rest.config;

import org.jasypt.digest.StandardStringDigester;
import org.jasypt.digest.StringDigester;
import org.jasypt.util.password.PasswordEncryptor;
import org.jasypt.util.password.StrongPasswordEncryptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
public class EncryptionConfig {

    /**
     * Provides {@code PasswordEncryptor} from Jasypt library.
     * <p>
     * Scope of the bean is {@code prototype} in order of thread safety guarantee
     *
     * @return new instance of PasswordEncryptor
     */
    @Bean
    @Scope(value = "prototype")
    public PasswordEncryptor providePasswordEncryptor() {
        return new StrongPasswordEncryptor();
    }

    @Bean
    @Scope(value = "prototype")
    public StringDigester provideStringDigester() {
        return new StandardStringDigester();
    }
}
