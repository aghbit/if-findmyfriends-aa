package pl.edu.bit.aa.service.exception;

import lombok.EqualsAndHashCode;
import lombok.Value;
import org.springframework.http.HttpStatus;
import pl.edu.bit.aa.rest.util.exception.JsonException;

@Value
@EqualsAndHashCode(callSuper = true)
@JsonException(HttpStatus.BAD_REQUEST)
public class FailedPasswordChangeException extends Exception {
    private final String reason = "INVALID_CREDENTIALS";
}
