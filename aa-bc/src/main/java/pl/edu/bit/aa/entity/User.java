package pl.edu.bit.aa.entity;

import lombok.*;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Indexed;
import org.mongodb.morphia.annotations.Reference;

import java.util.Collections;
import java.util.Set;
import java.util.UUID;

@Data
@EqualsAndHashCode(of = "id")
@RequiredArgsConstructor(staticName = "create")
@Entity
public class User {
    @Id
    private final UUID id;
    private final String name;
    private final String surname;
    @Indexed(unique = true, name = "email")
    private final String email;
    private final String password;
    private final boolean active = true;
    @Reference
    private final Set<Role> roles;

    // MongoDB purpose only
    private User() {
        this.id = null;
        this.name = null;
        this.surname = null;
        this.email = null;
        this.password = null;
        this.roles = Collections.emptySet();
    }

    public static User create(@NonNull String name, @NonNull String surname, @NonNull String email, @NonNull String password) {
        return new User(UUID.randomUUID(), name, surname, email, password, Collections.emptySet());
    }

    public Set<Role> getRoles() {
        return Collections.unmodifiableSet(roles);
    }
}
