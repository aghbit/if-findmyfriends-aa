package pl.edu.bit.aa.rest.config;

import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import pl.edu.bit.aa.entity.Permission;
import pl.edu.bit.aa.entity.Token;
import pl.edu.bit.aa.rest.util.Authorized;
import pl.edu.bit.aa.rest.util.RequestConstant;
import pl.edu.bit.aa.service.AuthorizationService;
import pl.edu.bit.aa.service.TokenService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Optional;

/**
 * Intercepts request for authorization purpose.
 * <p>
 * If controller method is annotated with {@link Authorized} annotation,
 * then {@code AuthorizationInterceptor} will extract auth token from request header, validate it (authenticate)
 * and then check if user related with token has {@link Permission} specified in {@link Authorized} annotation.
 * <p>
 * If auth token is invalid, interceptor will reject request with HTTP 401 code.
 * If user's permissions are insufficient, interceptor will reject request with HTTP 403 code.
 * If both above checks are passed, interceptor will allow controller to process request.
 */
@Component
public class AuthorizationInterceptor extends HandlerInterceptorAdapter {

    @Autowired
    private TokenService tokenService;

    @Autowired
    private AuthorizationService authorizationService;

    @Override
    public boolean preHandle(@NonNull HttpServletRequest request, @NonNull HttpServletResponse response,
                             @NonNull Object handler) throws Exception {
        Optional<Permission> requiredPermission = extractPermissionFrom(handler);
        if (requiredPermission.isPresent()) {
            Optional<Token> token = provideTokenFor(request);
            if (!token.isPresent()){
                response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                return false;
            } else {
                boolean userHasPermission = authorizationService.userHasPermission(
                        token.get().getUser().getId().toString(), requiredPermission.get().name());
                if (!userHasPermission) {
                    response.setStatus(HttpServletResponse.SC_FORBIDDEN);
                }
                return userHasPermission;
            }
        }
        return true;
    }

    private Optional<Permission> extractPermissionFrom(Object handler) {
        if (handler instanceof HandlerMethod) {
            HandlerMethod handlerMethod = (HandlerMethod) handler;
            Optional<Authorized> authorizedAnnotation = Optional.ofNullable(
                    handlerMethod.getMethodAnnotation(Authorized.class));
            if (authorizedAnnotation.isPresent()) {
                return Optional.ofNullable(authorizedAnnotation.get().value());
            }
        }
        return Optional.empty();
    }

    /**
     * Extracts token from request and tries to find it in DB.
     * @param request servletRequest
     * @return token if found or Optional.empty() otherwise
     */
    private Optional<Token> provideTokenFor(HttpServletRequest request) {
        return Optional.ofNullable(request.getHeader(RequestConstant.AUTH_HEADER))
                .flatMap(tokenService::findTokenAndUpdate);
    }
}
