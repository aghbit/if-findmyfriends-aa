package pl.edu.bit.aa.rest.json;

import lombok.RequiredArgsConstructor;
import lombok.Value;

import java.util.Set;

/**
 * @author Wojciech Milewski
 */
@Value
@RequiredArgsConstructor(staticName = "create")
public class RoleJson {
    private final String name;
    private final Set<String> permissions;
}
