package pl.edu.bit.aa.rest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import pl.edu.bit.aa.entity.Permission;
import pl.edu.bit.aa.entity.Role;
import pl.edu.bit.aa.rest.json.NewRoleJson;
import pl.edu.bit.aa.rest.json.RoleJson;
import pl.edu.bit.aa.rest.json.RolesListJson;
import pl.edu.bit.aa.rest.util.Authorized;
import pl.edu.bit.aa.service.AuthorizationService;
import pl.edu.bit.aa.service.RolesService;
import pl.edu.bit.aa.service.exception.AlreadyExistingRoleException;
import pl.edu.bit.aa.service.exception.NoSuchPermissionException;
import pl.edu.bit.aa.service.exception.RoleNotFoundException;
import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
class RolesController {

    @Autowired
    AuthorizationService authorizationService;

    @Autowired
    private RolesService rolesService;

    @RequestMapping(
            value = "/roles",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    @Authorized(Permission.ADMIN)
    public RolesListJson getAllRoles() {
        List<Role> roles = rolesService.getAllRoles();
        return RolesListJson.create(roles.stream().map(Role::getName).collect(Collectors.toList()));
    }

    @RequestMapping(
            value = "/roles/{name}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    @Authorized(Permission.ADMIN)
    public RoleJson getRole(@PathVariable String name) throws RoleNotFoundException {
        Role role = rolesService.get(name);
        return RoleJson.create(role.getName(), role.getPermissions().stream().map(Enum::name).collect(Collectors.toSet()));
    }

    @RequestMapping(
            value = "/roles",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )

    @ResponseStatus(HttpStatus.CREATED)
    @Authorized(Permission.ADMIN)
    public void createRole(@Valid @RequestBody NewRoleJson json)
            throws AlreadyExistingRoleException, NoSuchPermissionException {
        rolesService.createRole(json.getName(), json.getPermissions());
    }

    @RequestMapping(
            value = "/roles/{name}",
            method = RequestMethod.DELETE
    )
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @Authorized(Permission.ADMIN)
    public void deleteRole(@PathVariable String name) {
        rolesService.delete(name);
    }

    @RequestMapping(
            value = "/roles/{name}",
            method = RequestMethod.PUT,
            consumes = MediaType.APPLICATION_JSON_VALUE
    )
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @Authorized(Permission.ADMIN)
    public void updateRole(@PathVariable String name, @Valid @RequestBody NewRoleJson json)
            throws AlreadyExistingRoleException, NoSuchPermissionException, RoleNotFoundException {
        rolesService.update(name, json.getName(), json.getPermissions());
    }
}