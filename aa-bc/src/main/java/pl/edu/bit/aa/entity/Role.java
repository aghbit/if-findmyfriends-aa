package pl.edu.bit.aa.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;
import org.mongodb.morphia.annotations.Id;

import java.util.Collections;
import java.util.Set;

@Data
@EqualsAndHashCode(of = "name")
@RequiredArgsConstructor(staticName = "create")
public class Role {
    @Id
    private final String name;
    private final Set<Permission> permissions;

    /**
     * MongoDB purpose only. Do not use!
     */
    private Role() {
        name = "";
        permissions = Collections.emptySet();
    }

    public Set<Permission> getPermissions() {
        return Collections.unmodifiableSet(permissions);
    }
}
