package pl.edu.bit.aa.rest.json;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;

import java.util.Set;

@Value
public class CreatedUserJson {
    private final String email;
    private final String userId;

    public CreatedUserJson(@JsonProperty("email") String email, @JsonProperty("userId") String userId) {
        this.email = email;
        this.userId = userId;
    }
}