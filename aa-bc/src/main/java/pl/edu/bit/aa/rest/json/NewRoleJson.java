package pl.edu.bit.aa.rest.json;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;

import java.util.Set;

@Value
public class NewRoleJson {
    private final String name;
    private final Set<String> permissions;

    public NewRoleJson(@JsonProperty("name") String name, @JsonProperty("permissions") Set<String> permissions) {
        this.name = name;
        this.permissions = permissions;
    }
}
