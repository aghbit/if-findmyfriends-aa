package pl.edu.bit.aa.service

import org.jasypt.util.password.PasswordEncryptor
import pl.edu.bit.aa.db.RolesRepository
import pl.edu.bit.aa.db.UserRepository
import pl.edu.bit.aa.entity.Role
import pl.edu.bit.aa.entity.User
import pl.edu.bit.aa.service.exception.*
import spock.lang.Specification

class UserServiceTest extends Specification {

    static final OLD_PASSWORD = "ihavenoparents"
    static final NEW_PASSWORD = "iamnotthebatman"
    static final String ROLE_NAME = "testRole"

    UserRepository userRepository = Mock()
    RolesRepository rolesRepository = Mock()
    PasswordStrengthPolicy policy = Mock()
    PasswordEncryptor encryptor = Mock()

    UserService service = new UserService(userRepository, rolesRepository, policy, encryptor)
    UUID userUUID = UUID.randomUUID()
    String userId = userUUID.toString()



    def "User service should throw FailedPasswordChangeException when id of the user is not matching"() {
        given:
        userRepository.findById(_) >> Optional.empty()
        when:
        service.changePassword(userId, OLD_PASSWORD, NEW_PASSWORD)
        then:
        thrown(FailedPasswordChangeException)
    }

    def "User service should throw FailedPasswordChangeException when password does not match"() {
        given:
        User user = Mock()
        user.getId() >> userUUID
        user.getPassword() >> OLD_PASSWORD

        userRepository.findById(_) >> Optional.of(user)
        encryptor.checkPassword(_ as String, _ as String) >> false
        when:
        service.changePassword(userId, OLD_PASSWORD, NEW_PASSWORD)
        then:
        thrown(FailedPasswordChangeException)
    }

    def "User service should throw PasswordTooWeakException when new password is too weak"() {
        given:
        User user = Mock()
        user.getId() >> userUUID
        user.getPassword() >> OLD_PASSWORD

        userRepository.findById(_) >> Optional.of(user)
        encryptor.checkPassword(_ as String, _ as String) >> true

        policy.isPasswordStrong(_ as String) >> false
        when:
        service.changePassword(userId, OLD_PASSWORD, NEW_PASSWORD)
        then:
        thrown(PasswordTooWeakException)
    }

    def 'UserService should throw SamePasswordException when new password is same as the old one'() {
        given:
        User user = Mock()
        user.getId() >> userUUID
        user.getPassword() >> OLD_PASSWORD

        userRepository.findById(_) >> Optional.of(user)
        encryptor.checkPassword(OLD_PASSWORD, _ as String) >> true
        encryptor.checkPassword(_ as String, _ as String) >> true

        policy.isPasswordStrong(_ as String) >> true
        when:
        service.changePassword(userId, OLD_PASSWORD, OLD_PASSWORD)
        then:
        thrown(SamePasswordException)
    }

    def 'User service should encrypt new password and save new instance'() {
        given:
        User user = Mock()
        user.getId() >> userUUID
        user.getPassword() >> OLD_PASSWORD
        user.changePassword(_) >> user

        userRepository.findById(_) >> Optional.of(user)
        encryptor.checkPassword(_, _) >> true
        policy.isPasswordStrong(_) >> true

        when:
        service.changePassword(userId, OLD_PASSWORD, NEW_PASSWORD)
        then:
        1 * encryptor.encryptPassword(NEW_PASSWORD)
        1 * userRepository.save(_ as User)
    }

    def 'Changing role of non-existing user (invalid userId) should cause exception'() {
        given:
        userRepository.findById(userId) >> Optional.empty()
        when:
        service.assignRoleToUser(userId, ROLE_NAME)
        then:
        thrown(NoSuchUserException)
    }

    def 'Assignment of non-existing role should cause exception'() {
        given:
        def invalidRole = "roleSoInvalid"
        User user = Mock()
        userRepository.findById(userId) >> Optional.of(user)
        rolesRepository.findRoleByName(invalidRole) >> Optional.empty()
        when:
        service.assignRoleToUser(userId, invalidRole)
        then:
        thrown(NoSuchRoleException)
    }

    def 'Role assignment with proper parameters should result in successful role assignment'() {
        given:
        User user = Mock()
        user.getRoles() >> Collections.emptySet()
        user.getId() >> userUUID
        user.getName() >> "Name"
        user.getSurname() >> "Surname"
        user.getEmail() >> "email@email.com"
        user.getPassword() >> "hiperPassword"
        Role role = Mock()
        userRepository.findById(userId) >> Optional.of(user)
        rolesRepository.findRoleByName(ROLE_NAME) >> Optional.of(role)
        when:
        service.assignRoleToUser(userId, ROLE_NAME)
        then:
        1 * userRepository.save({ it.getRoles().contains(role) })
    }


}
