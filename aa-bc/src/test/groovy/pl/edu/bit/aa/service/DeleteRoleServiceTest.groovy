package pl.edu.bit.aa.service

import pl.edu.bit.aa.db.RolesRepository
import pl.edu.bit.aa.entity.Role
import spock.lang.Specification

class DeleteRoleServiceTest extends Specification {

    final static ROLE_NAME = "Batman"

    def rolesRepository = Mock(RolesRepository)

    def service = new RolesService(rolesRepository)

    def 'Roles service should be able to successfully delete existing role'() {
        given:
        def roleToDelete = Mock(Role)
        rolesRepository.findRoleByName(ROLE_NAME) >> Optional.of(roleToDelete)
        when:
        service.delete(ROLE_NAME)
        then:
        1 * rolesRepository.delete({it == roleToDelete})
    }

    def 'Roles service should do nothing when role has been already deleted'() {
        given:
        rolesRepository.findRoleByName(ROLE_NAME) >> Optional.empty()
        when:
        service.delete(ROLE_NAME)
        then:
        0 * rolesRepository.delete(_)
    }
}
