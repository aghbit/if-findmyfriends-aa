package pl.edu.bit.aa.service

import pl.edu.bit.aa.db.RolesRepository
import pl.edu.bit.aa.entity.Permission
import pl.edu.bit.aa.entity.Role
import pl.edu.bit.aa.service.exception.AlreadyExistingRoleException
import pl.edu.bit.aa.service.exception.NoSuchPermissionException
import spock.lang.Specification

class CreateRoleServiceTest extends Specification {

    final static ROLE_NAME = "Batman"
    final static PERMISSIONS = [Permission.TEST].toSet()
    final static PERMISSIONS_NAMES = PERMISSIONS.collect {it.name()}.toSet()

    def rolesRepository = Mock(RolesRepository)

    def service = new RolesService(rolesRepository)

    def 'Role service should be able to create a role successfully'() {
        given:
        rolesRepository.findRoleByName(ROLE_NAME) >> Optional.empty()
        when:
        service.createRole(ROLE_NAME, PERMISSIONS_NAMES)
        then:
        1 * rolesRepository.save({it.getName() == ROLE_NAME && it.getPermissions() == PERMISSIONS})
    }

    def 'Role service should throw an exception when role with such name already exists'() {
        given:
        rolesRepository.findRoleByName(ROLE_NAME) >> Optional.of(Mock(Role))
        when:
        service.createRole(ROLE_NAME, PERMISSIONS_NAMES)
        then:
        thrown(AlreadyExistingRoleException)
    }

    def 'Role service should throw an exception when some permission does not exist'() {
        given:
        rolesRepository.findRoleByName(ROLE_NAME) >> Optional.empty()
        when:
        service.createRole(ROLE_NAME, ["permissionWhichDoesNotExist"].toSet())
        then:
        def exception = thrown(NoSuchPermissionException)
        exception.getName() == "permissionWhichDoesNotExist"
    }

}
