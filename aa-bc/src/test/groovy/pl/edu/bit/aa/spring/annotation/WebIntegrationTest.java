package pl.edu.bit.aa.spring.annotation;

import org.springframework.test.context.ActiveProfiles;
import pl.edu.bit.aa.config.ApplicationProfile;

import java.lang.annotation.*;

@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)

@org.springframework.boot.test.WebIntegrationTest("server.port:0")
@ActiveProfiles(ApplicationProfile.TEST)
public @interface WebIntegrationTest {

}
