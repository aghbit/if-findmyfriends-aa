package pl.edu.bit.aa.rest

import com.jayway.restassured.RestAssured
import com.jayway.restassured.http.ContentType
import org.jasypt.util.password.PasswordEncryptor
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.test.SpringApplicationContextLoader
import org.springframework.http.HttpStatus
import org.springframework.test.context.ContextConfiguration
import pl.edu.bit.aa.Application
import pl.edu.bit.aa.db.TokenRepository
import pl.edu.bit.aa.db.UserRepository
import pl.edu.bit.aa.entity.Token
import pl.edu.bit.aa.entity.User
import pl.edu.bit.aa.rest.exception.InvalidMessageBodyException
import pl.edu.bit.aa.rest.util.RequestConstant
import pl.edu.bit.aa.rest.util.exception.ExceptionToJsonConverter
import pl.edu.bit.aa.service.exception.FailedPasswordChangeException
import pl.edu.bit.aa.service.exception.PasswordTooWeakException
import pl.edu.bit.aa.service.exception.SamePasswordException
import pl.edu.bit.aa.spring.annotation.WebIntegrationTest
import spock.lang.Specification

import javax.json.Json

import static com.jayway.restassured.RestAssured.given

@ContextConfiguration(loader = SpringApplicationContextLoader.class,
        classes = [Application.class])
@WebIntegrationTest
class PasswordChangeIT extends Specification {
    static final NAME = "Bruce"
    static final SURNAME = "Wayne"
    static final EMAIL = "bruce@wayne.com"
    static final OLD_PASSWORD = "ihavenoparents"
    static final NEW_PASSWORD = "iamnotthebatman"
    static final TOKEN = "1234"

    static final AUTH_TOKEN_HEADER = RequestConstant.AUTH_HEADER
    static final OLD_PASSWORD_JSON_FIELD = "oldPassword"
    static final NEW_PASSWORD_JSON_FIELD = "newPassword"

    @Value('${local.server.port}')
    int portNumber

    @Autowired
    UserRepository userRepository

    @Autowired
    TokenRepository tokenRepository;

    @Autowired
    PasswordEncryptor encryptor

    String bodyJson
    User user
    Token token

    void setup() {
        RestAssured.port = portNumber

        user = User.create(
                UUID.randomUUID(),
                NAME,
                SURNAME,
                EMAIL,
                encryptor.encryptPassword(OLD_PASSWORD),
        Collections.emptySet())

        userRepository.deleteAll()
        userRepository.save(user)

        token = Token.create(TOKEN, user)
        tokenRepository.save(token)

        bodyJson = prepareBodyJson()
    }

    void cleanup() {
        userRepository.deleteAll()
    }

    private static prepareBodyJson() {
        def jsonBuilder = Json.createObjectBuilder()
        jsonBuilder.add(OLD_PASSWORD_JSON_FIELD, OLD_PASSWORD)
        jsonBuilder.add(NEW_PASSWORD_JSON_FIELD, NEW_PASSWORD)
        return jsonBuilder.build().toString()
    }

    def "UserController should return proper message after successful password change"() {
        given:
        user
        def spec = given().contentType(ContentType.JSON).body(bodyJson).header(AUTH_TOKEN_HEADER, TOKEN)
        when:
        def response = spec.patch("/users/${user.getId().toString()}/password")
        then:
        HttpStatus.valueOf(response.statusCode()) == HttpStatus.NO_CONTENT
    }

    def "UserController should return proper code and body when user does not exist"() {
        given:
        userRepository.deleteAll()
        tokenRepository.deleteAll() // non existing user means no token for this user

        def spec = given().contentType(ContentType.JSON).body(bodyJson).header(AUTH_TOKEN_HEADER, TOKEN)
        when:
        def response = spec.patch("/users/${user.getId().toString()}/password")
        then:
        HttpStatus.valueOf(response.statusCode()) == HttpStatus.UNAUTHORIZED
    }

    def "UserController should return proper code and information when old password is missing in the request"() {
        given:

        def expectedResponseJson = ExceptionToJsonConverter.toJson(new InvalidMessageBodyException())

        def jsonBuilder = Json.createObjectBuilder()
        jsonBuilder.add("email", EMAIL)
        jsonBuilder.add("newPassword", NEW_PASSWORD)
        def bodyJson = jsonBuilder.build().toString()

        def spec = given().contentType(ContentType.JSON).body(bodyJson).header(AUTH_TOKEN_HEADER, TOKEN)
        when:
        def response = spec.patch("/users/${user.getId().toString()}/password")
        then:
        HttpStatus.valueOf(response.statusCode()) == HttpStatus.BAD_REQUEST
        response.asString() == expectedResponseJson
    }

    def "UserController should return proper code and information when new password is missing in the request"() {
        given:

        def expectedResponseJson = ExceptionToJsonConverter.toJson(new InvalidMessageBodyException());

        def jsonBuilder = Json.createObjectBuilder()
        jsonBuilder.add("email", EMAIL)
        jsonBuilder.add("oldPassword", OLD_PASSWORD)
        def bodyJson = jsonBuilder.build().toString()

        def spec = given().contentType(ContentType.JSON).body(bodyJson).header(AUTH_TOKEN_HEADER, TOKEN)
        when:
        def response = spec.patch("/users/${user.getId().toString()}/password")
        then:
        HttpStatus.valueOf(response.statusCode()) == HttpStatus.BAD_REQUEST
        response.asString() == expectedResponseJson
    }

    def "UserController should return proper code when there is no body in the request"() {
        def spec = given().contentType(ContentType.JSON).header(AUTH_TOKEN_HEADER, TOKEN)
        when:
        def response = spec.patch("/users/${user.getId().toString()}/password")
        def expectedResponse = ExceptionToJsonConverter.toJson(new InvalidMessageBodyException())
        then:
        HttpStatus.valueOf(response.statusCode()) == HttpStatus.BAD_REQUEST
        response.asString() == expectedResponse
    }

    def "UserController should return proper code when user is not authenticated"() {
        def invalidToken = 'iamnotvalidlol'
        def spec = given().contentType(ContentType.JSON).body(bodyJson).header(AUTH_TOKEN_HEADER, invalidToken)
        when:
        def response = spec.patch("/users/${user.getId().toString()}/password")
        then:
        HttpStatus.valueOf(response.statusCode()) == HttpStatus.UNAUTHORIZED
    }

    def "UserController should return proper code when there is no token in request"() {
        def spec = given().contentType(ContentType.JSON).body(bodyJson)
        when:
        def response = spec.patch("/users/${user.getId().toString()}/password")
        then:
        HttpStatus.valueOf(response.statusCode()) == HttpStatus.UNAUTHORIZED
    }

    def "UserController should return proper code when user is authenticated but password does not match"() {
        def jsonBuilder = Json.createObjectBuilder()
        jsonBuilder.add(OLD_PASSWORD_JSON_FIELD, "${OLD_PASSWORD}notmatching")
        jsonBuilder.add(NEW_PASSWORD_JSON_FIELD, NEW_PASSWORD)
        def bodyJson = jsonBuilder.build().toString()
        def spec = given().contentType(ContentType.JSON).body(bodyJson).header(AUTH_TOKEN_HEADER, TOKEN)
        def expectedResponse = ExceptionToJsonConverter.toJson(new FailedPasswordChangeException());
        when:
        def response = spec.patch("/users/${user.getId().toString()}/password")
        then:
        HttpStatus.valueOf(response.statusCode()) == HttpStatus.BAD_REQUEST
        response.asString() == expectedResponse
    }

    def "UserController should return BAD_REQUEST code and proper response when new password is too weak"() {
        given:

        def expectedResponseJson = ExceptionToJsonConverter.toJson(new PasswordTooWeakException())

        def jsonBuilder = Json.createObjectBuilder()
        jsonBuilder.add(OLD_PASSWORD_JSON_FIELD, OLD_PASSWORD)
        jsonBuilder.add(NEW_PASSWORD_JSON_FIELD, '123')
        def bodyJson = jsonBuilder.build().toString()

        def spec = given().contentType(ContentType.JSON).body(bodyJson).header(AUTH_TOKEN_HEADER, TOKEN)
        when:
        def response = spec.patch("/users/${user.getId().toString()}/password")
        then:
        HttpStatus.valueOf(response.statusCode()) == HttpStatus.BAD_REQUEST
        response.asString() == expectedResponseJson
    }

    def 'UserController should complain when new password is the same as the old one'() {
        given:

        def expectedResponseJson = ExceptionToJsonConverter.toJson(new SamePasswordException())

        def jsonBuilder = Json.createObjectBuilder()
        jsonBuilder.add(OLD_PASSWORD_JSON_FIELD, OLD_PASSWORD)
        jsonBuilder.add(NEW_PASSWORD_JSON_FIELD, OLD_PASSWORD)
        def bodyJson = jsonBuilder.build().toString()

        def spec = given().contentType(ContentType.JSON).body(bodyJson).header(AUTH_TOKEN_HEADER, TOKEN)
        when:
        def response = spec.patch("/users/${user.getId().toString()}/password")
        then:
        HttpStatus.valueOf(response.statusCode()) == HttpStatus.BAD_REQUEST
        response.asString() == expectedResponseJson
    }

}
