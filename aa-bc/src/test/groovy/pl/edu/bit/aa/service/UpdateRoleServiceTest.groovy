package pl.edu.bit.aa.service

import pl.edu.bit.aa.db.RolesRepository
import pl.edu.bit.aa.entity.Permission
import pl.edu.bit.aa.entity.Role
import pl.edu.bit.aa.service.exception.AlreadyExistingRoleException
import pl.edu.bit.aa.service.exception.NoSuchPermissionException
import pl.edu.bit.aa.service.exception.NoSuchRoleException
import pl.edu.bit.aa.service.exception.RoleNotFoundException
import spock.lang.Specification

class UpdateRoleServiceTest extends Specification {

    static final OLD_NAME = 'Batman'
    static final NEW_NAME = 'The Dark Knight'
    static final OLD_PERMISSIONS = [Permission.TEST].toSet()
    static final OLD_PERMISSIONS_NAMES = OLD_PERMISSIONS.collect {it.name()}.toSet()
    static final NEW_PERMISSIONS_NAMES = [].toSet()

    def rolesRepository = Mock(RolesRepository)

    def service = new RolesService(rolesRepository)

    def 'Service should be able to change name of role successfully'() {
        given:
        Role role = Mock(Role)
        rolesRepository.findRoleByName(OLD_NAME) >> Optional.of(role)
        rolesRepository.findRoleByName(NEW_NAME) >> Optional.empty()
        when:
        service.update(OLD_NAME, NEW_NAME, OLD_PERMISSIONS_NAMES)
        then:
        1 * rolesRepository.delete(role)
        1 * rolesRepository.save({it.getName() == NEW_NAME && it.getPermissions() == OLD_PERMISSIONS})
    }

    def 'Service should be able to change permissions of role successfully'() {
        given:
        Role role = Mock(Role)
        rolesRepository.findRoleByName(OLD_NAME) >> Optional.of(role)
        rolesRepository.findRoleByName(NEW_NAME) >> Optional.empty()
        when:
        service.update(OLD_NAME, OLD_NAME, NEW_PERMISSIONS_NAMES)
        then:
        0 * rolesRepository.delete(role)
        1 * rolesRepository.save({it.getName() == OLD_NAME && it.getPermissions() == NEW_PERMISSIONS_NAMES})
    }

    def 'Service should prevent changing role name to the existing one by throwing an exception'() {
        given:
        Role role = Mock(Role)
        rolesRepository.findRoleByName(OLD_NAME) >> Optional.of(role)
        rolesRepository.findRoleByName(NEW_NAME) >> Optional.of(Mock(Role))
        when:
        service.update(OLD_NAME, NEW_NAME, NEW_PERMISSIONS_NAMES)
        then:
        thrown(AlreadyExistingRoleException)
    }

    def 'Service should prevent changing role when it does not exist'() {
        given:
        rolesRepository.findRoleByName(OLD_NAME) >> Optional.empty()
        when:
        service.update(OLD_NAME, NEW_NAME, NEW_PERMISSIONS_NAMES)
        then:
        thrown(RoleNotFoundException)
    }

    def 'Service should throw an exception when some permission does not exist'() {
        given:
        Role role = Mock(Role)
        rolesRepository.findRoleByName(OLD_NAME) >> Optional.of(role)
        rolesRepository.findRoleByName(NEW_NAME) >> Optional.empty()
        when:
        service.update(OLD_NAME, NEW_NAME, ["permissionWhichDoesNotExist"].toSet())
        then:
        def exception = thrown(NoSuchPermissionException)
        exception.getName() == "permissionWhichDoesNotExist"
    }
}
