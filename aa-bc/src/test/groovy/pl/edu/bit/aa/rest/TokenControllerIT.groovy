package pl.edu.bit.aa.rest

import com.jayway.restassured.RestAssured
import com.jayway.restassured.http.ContentType
import org.jasypt.util.password.PasswordEncryptor
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.test.SpringApplicationContextLoader
import org.springframework.http.HttpStatus
import org.springframework.test.context.ContextConfiguration
import pl.edu.bit.aa.Application
import pl.edu.bit.aa.db.TokenRepository
import pl.edu.bit.aa.db.UserRepository
import pl.edu.bit.aa.entity.Token
import pl.edu.bit.aa.entity.User
import pl.edu.bit.aa.rest.exception.InvalidMessageBodyException
import pl.edu.bit.aa.rest.json.TokenWithUserIdJson
import pl.edu.bit.aa.rest.json.TokenJson
import pl.edu.bit.aa.rest.util.RequestConstant
import pl.edu.bit.aa.rest.util.exception.ExceptionToJsonConverter
import pl.edu.bit.aa.service.exception.InvalidCredentialsException
import pl.edu.bit.aa.spring.annotation.WebIntegrationTest
import spock.lang.Specification

import javax.json.Json

import static com.jayway.restassured.RestAssured.given

@ContextConfiguration(loader = SpringApplicationContextLoader.class, classes = [Application.class])
@WebIntegrationTest
class TokenControllerIT extends Specification {

    private static final String tokenAuthPath = "/authenticate"
    private static final String tokenRequestPath = "/request-token"
    private static final String loginPath = "/login"
    private static final String logoutPath = "/logout"
    private static final String token = "123"
    private static final String userId = UUID.randomUUID().toString()
    private static final String name = "John"
    private static final String surname = "Cena"
    private static final String email = "mejlinator@gmail.com"
    private static final String password = "1234567"

    @Value('${local.server.port}')
    private int portNumber

    @Autowired
    private TokenRepository tokenRepository

    @Autowired
    private UserRepository userRepository

    @Autowired
    private PasswordEncryptor encryptor;

    def setup() {
        RestAssured.port = portNumber
        tokenRepository.deleteAll()
        userRepository.deleteAll()
    }

    def cleanup() {
        tokenRepository.deleteAll()
        userRepository.deleteAll()
    }

    def "Invalid request body when obtaining token cause proper status and message"() {
        given:
        def messedUpBody = Json.createObjectBuilder().add("sdasdas", "2121212121").build().toString()
        def emptySpec = given().contentType(ContentType.JSON) // empty body
        def messedUpSpec = given().contentType(ContentType.JSON).body(messedUpBody)
        def expectedEmptyBodyAnswer = ExceptionToJsonConverter.toJson(new InvalidMessageBodyException())
        def expectedMessedBodyAnswer = ExceptionToJsonConverter.toJson(new InvalidMessageBodyException())
        when:
        def response1 = emptySpec.post(tokenAuthPath)
        def response2 = messedUpSpec.post(tokenAuthPath)
        then:
        HttpStatus.valueOf(response1.statusCode()) == HttpStatus.BAD_REQUEST
        HttpStatus.valueOf(response2.statusCode()) == HttpStatus.BAD_REQUEST
        response1.asString() == expectedEmptyBodyAnswer
        response2.asString() == expectedMessedBodyAnswer
    }

    def "Token not found should cause proper status"() {
        given:
        def properJsonBody = Json.createObjectBuilder().add("token", token).add("userId", userId)
                .build().toString()
        def spec = given().contentType(ContentType.JSON).body(properJsonBody)
        when:
        def result = spec.post(tokenAuthPath)
        then:
        HttpStatus.valueOf(result.statusCode()) == HttpStatus.UNAUTHORIZED
    }

    def "Query for existing token should cause proper status"() {
        given:
        User zenek = User.create(UUID.randomUUID(), name, surname, email, password, Collections.emptySet())
        userRepository.save(zenek)
        tokenRepository.save(Token.create(token, zenek))
        String properBody = Json.createObjectBuilder()
                .add("token", token)
                .build().toString()
        def spec = given().contentType(ContentType.JSON).body(properBody)
        when:
        def result = spec.post(tokenAuthPath)
        then:
        HttpStatus.valueOf(result.statusCode()) == HttpStatus.OK
    }

    def "Query for token creation with missing request parameters should cause proper status and response"() {
        given:
        def noUserIdBody = Json.createObjectBuilder().add("wrongField", "val").add("password", "password").build().toString()
        def noPasswordBody = Json.createObjectBuilder().add("userId", userId).add("wrongField", "val").build().toString()
        def noUserIdSpec = given().contentType(ContentType.JSON).body(noUserIdBody)
        def noPasswordSpec = given().contentType(ContentType.JSON).body(noPasswordBody)
        def expectedNoUserIdAnswer = ExceptionToJsonConverter.toJson(new InvalidMessageBodyException())
        def expectedNoPasswordAnswer = ExceptionToJsonConverter.toJson(new InvalidMessageBodyException())
        when:
        def response1 = noUserIdSpec.post(tokenRequestPath)
        def response2 = noPasswordSpec.post(tokenRequestPath)
        then:
        HttpStatus.valueOf(response1.statusCode()) == HttpStatus.BAD_REQUEST
        HttpStatus.valueOf(response2.statusCode()) == HttpStatus.BAD_REQUEST
        response1.asString() == expectedNoUserIdAnswer
        response2.asString() == expectedNoPasswordAnswer
    }

    def "Proper token request should create new token and generate proper response"() {
        given:
        User user = User.create(UUID.randomUUID(), name, surname, email, encryptor.encryptPassword(password), [].toSet())
        userRepository.save(user)
        def properRequestBody = Json.createObjectBuilder()
                .add("userId", user.getId().toString())
                .add("password", password)
                .build().toString()
        def spec = given().contentType(ContentType.JSON).body(properRequestBody)
        when:
        def response = spec.post(tokenRequestPath)
        then:
        HttpStatus.valueOf(response.statusCode()) == HttpStatus.OK
        def newToken = response.body().as(TokenJson.class).getToken()
        tokenRepository.findTokenAndUpdate(newToken).isPresent()
        def expectedResponse = Json.createObjectBuilder().add("token", newToken).build().toString()
        response.asString() == expectedResponse
    }

    def "User should be able to log in"() {
        given:
        User user = User.create(UUID.randomUUID(), name, surname, email, encryptor.encryptPassword(password), [].toSet())
        userRepository.save(user)
        def properRequestBody = Json.createObjectBuilder()
                .add("name", name)
                .add("surname", surname)
                .add("email", email)
                .add("password", password)
                .build().toString()
        def spec = given().contentType(ContentType.JSON).body(properRequestBody)
        when:
        def response = spec.post(loginPath)
        then:
        HttpStatus.valueOf(response.statusCode()) == HttpStatus.OK
        def newToken = response.body().as(TokenWithUserIdJson.class).getToken()
        tokenRepository.findTokenAndUpdate(newToken).isPresent()
        def expectedResponse = Json.createObjectBuilder()
                .add("token", newToken)
                .add("userId", user.getId().toString())
                .build().toString()
        response.asString() == expectedResponse
    }

    def "Login attempt with invalid credentials should result in proper status and response"() {
        given:
        User user = User.create(UUID.randomUUID(), name, surname, email, encryptor.encryptPassword(password), [].toSet())
        userRepository.save(user)
        def requestBody = Json.createObjectBuilder()
                .add("name", name)
                .add("surname", surname)
                .add("email", email)
                .add("password", password + "invalid")
                .build().toString()
        def spec = given().contentType(ContentType.JSON).body(requestBody)
        def expectedResponse = ExceptionToJsonConverter.toJson(new InvalidCredentialsException())
        when:
        def response = spec.post(loginPath)
        then:
        HttpStatus.valueOf(response.statusCode()) == HttpStatus.UNAUTHORIZED
        response.asString() == expectedResponse
    }

    def "Login attempt as not existing user should result in proper status and response"() {
        given:
        def requestBody = Json.createObjectBuilder()
                .add("name", name)
                .add("surname", surname)
                .add("email", email)
                .add("password", password)
                .build().toString()
        def spec = given().contentType(ContentType.JSON).body(requestBody)
        def expectedResponse = ExceptionToJsonConverter.toJson(new InvalidCredentialsException())
        when:
        def response = spec.post(loginPath)
        then:
        HttpStatus.valueOf(response.statusCode()) == HttpStatus.UNAUTHORIZED
        response.asString() == expectedResponse
    }

    def "User should be able to log out"() {
        given:
        User user = User.create(UUID.randomUUID(), name, surname, email, password, Collections.emptySet())
        userRepository.save(user)
        Token activeToken = Token.create(token, user)
        tokenRepository.save(activeToken)
        def spec = given().header(RequestConstant.AUTH_HEADER, token)
        when:
        def response = spec.post(logoutPath)
        then:
        HttpStatus.valueOf(response.getStatusCode()) == HttpStatus.NO_CONTENT
    }

    def "User without valid token should not be able to log out"() {
        given:
        User user = User.create(UUID.randomUUID(), name, surname, email, password, Collections.emptySet())
        userRepository.save(user)
        def spec = given().header(RequestConstant.AUTH_HEADER, token)
        when:
        def response = spec.post(logoutPath)
        then:
        HttpStatus.valueOf(response.getStatusCode()) == HttpStatus.UNAUTHORIZED
    }

}
