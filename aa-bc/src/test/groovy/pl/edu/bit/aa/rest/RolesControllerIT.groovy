package pl.edu.bit.aa.rest

import com.jayway.restassured.RestAssured
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.test.SpringApplicationContextLoader
import org.springframework.http.HttpStatus
import org.springframework.test.context.ContextConfiguration
import pl.edu.bit.aa.Application
import pl.edu.bit.aa.db.RolesRepository
import pl.edu.bit.aa.db.TokenRepository
import pl.edu.bit.aa.db.UserRepository
import pl.edu.bit.aa.entity.Permission
import pl.edu.bit.aa.entity.Role
import pl.edu.bit.aa.entity.Token
import pl.edu.bit.aa.entity.User
import pl.edu.bit.aa.rest.util.RequestConstant
import pl.edu.bit.aa.spring.annotation.WebIntegrationTest
import spock.lang.Specification

import javax.json.Json

import static com.jayway.restassured.RestAssured.get
import static com.jayway.restassured.RestAssured.given

@ContextConfiguration(loader = SpringApplicationContextLoader.class,
        classes = [Application.class])
@WebIntegrationTest
class RolesControllerIT extends Specification {

    static final TOKEN = "1234"

    static final ROLES_FIELD = "roles"
    static final AUTH_HEADER = RequestConstant.AUTH_HEADER

    static final ADMIN_NAME = "adminname"
    static final USER_NAME = "username"
    static final ADMIN_SURNAME = "adminsurname"
    static final USER_SURNAME = "usersurname"
    static final ADMIN_EMAIL = "admin@email.com"
    static final USER_EMAIL ="user@email.com"
    static final PASSWORD = "hiperPassword"
    static final UUID ADMIN_UUID = UUID.randomUUID()
    static final UUID USER_UUID = UUID.randomUUID()
    static final TEST_ROLE = "testRole"

    static final ADMIN_ROLE = "ADMIN"
    static final USER_ROLE = "USER"

    Role adminRole
    Role roleToBeAssigned
    User admin
    User user
    Token token

    @Value('${local.server.port}')
    int portNumber

    @Autowired
    RolesRepository rolesRepository

    @Autowired
    UserRepository userRepository

    @Autowired
    TokenRepository tokenRepository

    void setup() {
        RestAssured.port = portNumber
        rolesRepository.deleteAll()
        userRepository.deleteAll()
        tokenRepository.deleteAll()

        adminRole = Role.create(ADMIN_ROLE, Collections.singleton(Permission.ADMIN))
        roleToBeAssigned = Role.create(TEST_ROLE, Collections.singleton(Permission.TEST))
        rolesRepository.save(adminRole)
        rolesRepository.save(roleToBeAssigned)
        admin = User.create(ADMIN_UUID, ADMIN_NAME, ADMIN_SURNAME, ADMIN_EMAIL, PASSWORD, Collections.singleton(adminRole))
        user = User.create(USER_UUID, USER_NAME, USER_SURNAME, USER_EMAIL, PASSWORD, Collections.emptySet()) // user has no roles
        userRepository.save(admin)
        userRepository.save(user)
        token = Token.create(TOKEN, admin)
        tokenRepository.save(token)
    }

    void cleanup() {
        rolesRepository.deleteAll()
        userRepository.deleteAll()
        tokenRepository.deleteAll()
    }

    def "RolesController should return proper body after successful getting list of roles request"() {
        given:
        def userRole = Role.create(USER_ROLE, Collections.emptySet())
        rolesRepository.save(userRole)

        def spec = given().header(AUTH_HEADER, TOKEN)

        def expectedResponse = Json.createObjectBuilder()
                .add(ROLES_FIELD, Json.createArrayBuilder()
                .add(ADMIN_ROLE)
                .add(TEST_ROLE)
                .add(USER_ROLE))
                .build().toString()
        when:
        def response = spec.get("/roles")
        then:
        HttpStatus.OK == HttpStatus.valueOf(response.statusCode())
        expectedResponse == response.getBody().asString()

    }

    def "RolesController should return proper info when user is not authenticated"() {
        given:
        tokenRepository.deleteAll()
        when:
        def response = get('/roles')
        then:
        HttpStatus.UNAUTHORIZED == HttpStatus.valueOf(response.statusCode())
    }

    def "RolesController should return proper info when user is not authorized"() {
        given:
        userRepository.deleteAll()
        admin = User.create(ADMIN_UUID, ADMIN_NAME, ADMIN_SURNAME, ADMIN_EMAIL, PASSWORD, Collections.emptySet())
        userRepository.save(admin)
        when:
        def response = get('/roles')
        then:
        HttpStatus.UNAUTHORIZED == HttpStatus.valueOf(response.statusCode())
    }

}
