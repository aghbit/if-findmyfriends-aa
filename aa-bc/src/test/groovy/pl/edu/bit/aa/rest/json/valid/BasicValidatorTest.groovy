package pl.edu.bit.aa.rest.json.valid

import org.springframework.validation.Errors
import pl.edu.bit.aa.rest.exception.InvalidMessageBodyException
import spock.lang.Specification

/**
 * See {@link ClassStubJson} to see what functions will not be called during validation.
 */
class BasicValidatorTest extends Specification {

    Errors errors = Mock()
    ClassStubJson json;

    BasicValidator validator = new BasicValidator();

    def 'Validator should notice invalid fields'(){
        given:
        json = new ClassStubJson(null, 5)
        when:
        validator.validate(json, errors)
        then:
        thrown(InvalidMessageBodyException)

    }

    def 'Validator should pass request with appropriate data'(){
        given:
        json = new ClassStubJson("janek", null) // Integer can be null, only Strings fields are validated
        when:
        validator.validate(json, errors)
        then:
        notThrown(InvalidMessageBodyException)
    }
}
