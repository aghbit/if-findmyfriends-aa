package pl.edu.bit.aa.test.factory

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import pl.edu.bit.aa.db.RolesRepository
import pl.edu.bit.aa.db.TokenRepository
import pl.edu.bit.aa.db.UserRepository
import pl.edu.bit.aa.entity.Permission
import pl.edu.bit.aa.entity.Role
import pl.edu.bit.aa.entity.Token
import pl.edu.bit.aa.entity.User

@Component
class DataFactory {

    private static final USER = [name: 'Joker', surname: 'Evil', mail:'joker69', password: 'batmanhasnoparents', token: '1234']
    private static final ADMIN = [name: 'Bat', surname: 'Man', mail: 'battie13', password: 'justice', token: '5678']

    @Autowired
    private UserRepository userRepository

    @Autowired
    private TokenRepository tokenRepository

    @Autowired
    private RolesRepository rolesRepository

    private Role adminRole

    private User user
    private User admin

    def flushDb() {
        rolesRepository.deleteAll()
        tokenRepository.deleteAll()
        userRepository.deleteAll()
    }

    def prepareData() {
        prepareRoles()
        prepareUsers()
        prepareTokens()
    }

    private def prepareRoles() {
        adminRole = Role.create("ADMIN", [Permission.ADMIN].toSet())
        rolesRepository.save(adminRole)
    }

    private def prepareUsers() {
        user = User.create(UUID.randomUUID(), USER.name, USER.surname, USER.mail, USER.password, [].toSet())
        admin = User.create(UUID.randomUUID(), ADMIN.name, ADMIN.surname, ADMIN.mail, ADMIN.password, [adminRole].toSet())

        userRepository.save(user)
        userRepository.save(admin)
    }

    private def prepareTokens() {
        Token userToken = Token.create(USER.token, user)
        Token adminToken = Token.create(ADMIN.token, admin)

        tokenRepository.save(userToken)
        tokenRepository.save(adminToken)
    }

    static def getUserToken() {
        return USER.token
    }

    static def getAdminToken() {
        return ADMIN.token
    }

    def injectRoleWithName(String name) {
        rolesRepository.save(Role.create(name, [Permission.TEST].toSet()))
    }

    def flushTokens() {
        tokenRepository.deleteAll()
    }

    def flushRoles() {
        rolesRepository.deleteAll()
    }
}
