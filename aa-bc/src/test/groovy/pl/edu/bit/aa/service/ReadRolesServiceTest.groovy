package pl.edu.bit.aa.service

import pl.edu.bit.aa.db.RolesRepository
import pl.edu.bit.aa.entity.Permission
import pl.edu.bit.aa.entity.Role
import pl.edu.bit.aa.service.exception.NoSuchRoleException
import pl.edu.bit.aa.service.exception.RoleNotFoundException
import spock.lang.Specification

class ReadRolesServiceTest extends Specification {

    static final ROLE_NAME = "Batman"

    RolesRepository rolesRepository = Mock();

    RolesService rolesService = new RolesService(rolesRepository)

    def 'Fetching list of all roles should be possible'() {
        given:
        def rolesList = [Mock(Role), Mock(Role), Mock(Role)]
        rolesRepository.findAll() >> rolesList
        when:
        def resultList = rolesService.getAllRoles()
        then:
        rolesList == resultList
    }

    def 'Service should be able to read one specific role'() {
        given:
        def role = Mock(Role)
        rolesRepository.findRoleByName(ROLE_NAME) >> Optional.of(role)
        when:
        def result = rolesService.get(ROLE_NAME)
        then:
        result == role
    }

    def 'Service should throw an exception when there is no role when requested'() {
        given:
        rolesRepository.findRoleByName(ROLE_NAME) >> Optional.empty()
        when:
        rolesService.get(ROLE_NAME)
        then:
        thrown(RoleNotFoundException)
    }

}
